#include "plansza.hh"

int main(){
    int poziom=0;
    int tryb=2;
    cout<<"Wybierz tryb gry: \n 1 - Player vs COM\n 2 - Player vs Player\n";
    cin>>tryb;
    if(tryb==1){
        cout<<"Wybierz poziom trudnosci (ile ruchow w glab widzi komputer, musisz sie liczyc, ze im wiekszy poziom trudnosci, tym komputer potrzebuje wiecej czasu ;))"<<endl;
        cin>>poziom;
    }
    KtoZaczyna(); 
    Plansza plansza;
    plansza.Wyswietl();
    while(1){
        char kto;
        WyborPola(plansza,poziom,tryb);
        plansza.Wyswietl();
        int wynik=SprawdzWynik(plansza);
        if(wynik==10 || wynik==-10){
            if(wynik==-10) kto='O';
            if(wynik==10) kto='X';
            cout<<"Wygrywa "<<kto<<"!"<<endl;
            int zagrajponownie=ZagrajPonownie();
            if(zagrajponownie==1){
                plansza.Wyczysc();
                plansza.Wyswietl(); 
            }
            else if(zagrajponownie==2){
                tryb=UstawTryb();
                poziom=UstawPoziomSI(tryb);
                plansza.UstawParametry();
                plansza.Wyczysc();
                plansza.Wyswietl(); 
            }
            else
                exit(0);
            
        }
        else if(wynik == 0 && plansza.CzyPelna() == 1){
            cout<<"Remis"<<endl;
            int zagrajponownie=ZagrajPonownie();
            if(zagrajponownie==1){
                plansza.Wyczysc();
                plansza.Wyswietl();  
            }
            else if(zagrajponownie==2){
                tryb=UstawTryb();
                poziom=UstawPoziomSI(tryb);
                plansza.Wyczysc();
                plansza.Wyswietl(); 
               
            }
            else  
                exit(0);

        }
    }
    
    


    return 0;
}