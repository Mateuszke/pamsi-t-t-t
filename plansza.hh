#ifndef PLANSZA_HH
#define PLANSZA_HH

#include <vector>
#include <iostream>
#include <math.h>
#include <memory>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <cstdio>

using namespace std;
//class Plansza;
char players[2]={'O','X'};
char player;

//Funkcja pozwala użytkownikowi ustawienie trybu gry, czy gra przeciwko innemu graczowi czy komputerowi
int UstawTryb(){
    int tryb;
    cout<<"Wybierz tryb gry: \n 1 - Player vs COM\n 2 - Player vs Player\n";
    cin>>tryb;
    return tryb;
}
//Funkcja pozwalająca ustawić poziom trudności, tj. ustawić maksymalną głębokość dla algorytmu minimax
int UstawPoziomSI(int tryb){
    if(tryb==1){
        int poziom;
        cout<<"Wybierz poziom trudnosci (ile ruchow w glab widzi komputer, musisz sie liczyc, ze im wiekszy poziom trudnosci, tym komputer potrzebuje wiecej czasu ;))"<<endl;
        cin>>poziom;
        return poziom;
    }
    else return 0;
}

//Funkcja zmieniająca gracza po zakończeniu ruchu
void ZmienGracza(){
        if(player=='O')
            player='X';
        else player='O';
}


//Losowy wybór czy zaczyna kółko-gracz, czy krzyżyk-komputer
void KtoZaczyna(){
    srand(time(NULL));
    int ind=rand()%2;
    player=players[ind];
}

//Funkcja sprawdzająca czy kontynuować grę od nowa po końcu gry
int ZagrajPonownie(){
        char zagrajponownie;
        cout<<"Jeszcze raz? [t/n/z- zmien tryb]"<<endl;
        cin>>zagrajponownie;
        if(zagrajponownie=='t'){
            return 1;
        }
        else if(zagrajponownie=='z'){
            return 2;
        }
        else {
            return 0;
        }
        
}

//Klasa Plansza zawiera rozmiar planszy oraz zawartość planszy w kontenerze vector
class Plansza{
    public:
    
    vector<vector<char>> board;
    int rozmiar;
    //int rzad;
    
    //konstruktor
    Plansza(){
       
        UstawParametry();
        Wyczysc();
        
    }

    //Funkcja czyszcząca plansze
    void Wyczysc(){
        board.clear();
        int licznik=1;
        for(int i=0;i<rozmiar;i++){
            
            vector<char> pom;
            for(int j=0;j<rozmiar;j++){
                char pole ='.';
                pom.push_back(pole);
                licznik++;
            }
            board.push_back(pom);
        }
        
    }
    
    //Funkcja wyświetlająca planszę
    void Wyswietl(){
        int licznikrzedow=1;
        cout<<"   ";
        for(int l=1;l<=rozmiar;l++){
            if(l%10==0&& l!=0){
                cout<<l/10<<" ";
            }
            else cout<<"  ";
        }
        cout<<endl;
        cout<<"   ";
        for(int k=1;k<=rozmiar;k++){
            cout<<k%10<<' ';
        }
        cout<<endl;
        for(int i=0;i<board.size();i++){
            if(licznikrzedow<10)
                cout<<" ";
            cout<<licznikrzedow<<" ";
            for(int j=0;j<board[i].size();j++){
               
                cout<<board[i][j]<<" ";
            }
            cout<<endl;
            licznikrzedow++;
        }
    }
    
    
    
    //Funkcja ustawienia opcji gry
    void UstawParametry(){
        cout<<"Wprowadz rozmiar planszy: ";
        cin>>rozmiar;
        while(cin.fail()){
            cout<<"Blad, spróbuj ponownie: "<<endl;
            cin.clear();cin.ignore(1000,'\n');
            cin>> rozmiar;
        }
        // cout<<"Wprowadz ilosc w rzedzie potrzebne do wygranej: ";
        // cin>>rzad;
        // while(cin.fail()){
        //     cout<<"Blad, sprobuj ponownie: "<<endl;
        //     cin.clear();cin.ignore(1000,'\n');
        //     cin>> rzad;
        // }
    }

    //Funkcja sprawdzająca czy na planszy nie ma wolnych miejsc
    bool CzyPelna(){
        int wolne=0;
        for(int i=0;i<board.size();i++){
            for(int j=0;j<board.size();j++){
                if(board[i][j]=='.')
                    wolne++;
            }
        }
        if(wolne<=0){
            return true;
        }
        else  return false;
    }

    
};

//Funkcja sprawdzająca czy aktualny stan planszy zapewnia wygraną którejś ze stron (zwraca 10 jeśli wygrał X  i  -10 jeśli O) oraz czy jest remis
int SprawdzWynik(Plansza &plansza){
        int winner=100;
        int score;
        //w poziomie
        for(int i=0;i<plansza.board.size();i++){
            score=0;
            for(int j=0;j<plansza.board.size();j++){
                if(plansza.board[i][j]=='O'){
                    score-=1;
                }
                if(plansza.board[i][j]=='X'){
                    score+=1;
                }
            }
            if(score==-(plansza.board.size())){
                winner=-10; //wygrana O !max
                break;
            }
            if(score==plansza.board.size()){
                winner=10; //wygrana X max
                break;
            }
        }
        score=0;
        //w pionie
        for(int i=0;i<plansza.board.size();i++){
            int score=0;
            for(int j=0;j<plansza.board.size();j++){
                if(plansza.board[j][i]=='O'){
                    score-=1;
                }
                if(plansza.board[j][i]=='X'){
                    score+=1;
                }
            }
            if(score== -(plansza.board.size())){ //O
                winner=-10;
                break;
            }
            if(score == plansza.board.size()){ //X
                winner=10;
                break;
            }
        }
        score=0;
        //przekatna 1
        for(int i=0;i<plansza.board.size();i++){
            if(plansza.board[i][i]=='O'){
                score-=1;
            }
            if(plansza.board[i][i]=='X'){
                score+=1;
            }
            if(score==-(plansza.board.size())){//O
                winner=-10;
                break;
            }
            if(score==plansza.board.size()){//X
                winner= 10;
                break;
            }
        }
        score=0;
        //przekatna 2
        int i=plansza.board.size()-1;score=0;
        for(int j=0;j<plansza.board.size();j++){
            if(plansza.board[i][j]=='O'){
                score-=1;
            }
            if(plansza.board[i][j]=='X'){
                score+=1;
            }
            if(score==-(plansza.board.size())){
                winner=-10;
                break;
            }
            if(score==plansza.board.size()){
                winner= 10;
                break;
            }
            i=i-1;
        }
        //jesli plansza pelna i nikt nie zostal zwyciesca to zwroc 0 - remis
        if(plansza.CzyPelna()==1 && winner==100){
            winner= 0;
        }
        return winner;
}

//algorytm minimax
float minimax(Plansza &plansza, int initialDepth, int depth, bool isMaximizing, float alfa, float beta){
        
        //sprawdz wynik w obecnej pozycji
        float result=SprawdzWynik(plansza);
        
        if(depth<1){        //jesli dojdziemy do konca głębokości
            return 0;
        }
        //jesli to jest koncowa pozycja to zwroc rezultat podzielony przez glebokosc tego ruchu
        if (result == 10 || result == -10 || result==0 ){
            return result/((initialDepth+1)-depth);
        }
        
        
        
        //dla X, szukajacego najwiekszego wyniku
        if(isMaximizing){
            float maxBestScore=-1000000;
            for(int i=0;i<plansza.board.size();i++){
                for(int j=0;j<plansza.board.size();j++){
                    if(plansza.board[i][j]=='.'){
                        plansza.board[i][j]='X'; //wstaw do pozycji
                        float score=minimax(plansza, initialDepth,  depth-1, false, alfa, beta); //szukaj wgłąb
                        plansza.board[i][j]='.';//wyczysc do oryginalnego stanu
                        maxBestScore=max(score,maxBestScore); //wez bardziej korzystny
                        alfa=max(alfa,score);  //ciecia alfabeta
                        if (beta<=alfa){
                            break;
                        }                  
                    }
                }
            }
            return maxBestScore; //zwroc najwiekszy znaleziony wynik
        }
        //dla O - szukajacego jak najmniejszego wyniku - analogicznie jak dla X
        else {
            float minBestScore=1000000;
            for(int i=0;i<plansza.board.size();i++){
                for(int j=0;j<plansza.board.size();j++){
                    if(plansza.board[i][j]=='.'){
                        plansza.board[i][j]='O';
                        float score=minimax(plansza, initialDepth, depth-1, true, alfa, beta);
                        plansza.board[i][j]='.';
                        minBestScore=min(score,minBestScore);
                        beta=min(score, beta);
                        if(beta<=alfa){         //ciecia alfabeta
                            break;
                        }
                    }
                }
            }
            return minBestScore; //zwroc najmniejszy znaleziony
        }      
       
    }


//Funkcja która pozwala graczowi wybrac pole, gdzie wstawic O, oraz uruchamiająca algorytmy dla komputera
void WyborPola(Plansza &plansza, int difficulty, int tryb){
    int a,b;
    cout<<"Ruch "<<player<<", wybierz rzad i kolumne: ";
    
    //gdy gracz
    if(player=='O'){
        
        while(1){
            int blad=0;
            cin>>a;
            cin>>b;
            if(a>plansza.board.size() || b>plansza.board.size() || a<1 || b<1){
                cout<<"Nieprawidlowe pole!"<<endl;
                blad=1;          
            }
            if(blad==0){
                if(plansza.board[a-1][b-1]!='.'){
                    cout<<"Pole jest jest juz zajete, wybierz inne. "<<endl;
                    
                }
                if(plansza.board[a-1][b-1]=='.'){
                    plansza.board[a-1][b-1]=player;
                    break;
                }
            }   
        }
    }
    
    if(player=='X'){
        //gdy komputer
        if(tryb==1){
            cout<<"Mysle..."<<endl;
            int bestScore=-1000000;
            int x=0;
            int y=0;
            for(int i=0;i<plansza.board.size();i++){
                for(int j=0;j<plansza.board.size();j++){
                    if(plansza.board[i][j]=='.'){
                        plansza.board[i][j]='X';
                        float score=minimax( plansza, difficulty, difficulty, false, -100000, +100000);
                        plansza.board[i][j]='.';
                        if(score>bestScore){
                            bestScore=score;
                            x=i;y=j;
                        }
                    }
                }
            }
            
            cout<<x<<' '<<y<<endl;
            plansza.board[x][y]='X';
        }
        //gdy gracz
        else if(tryb==2){
            while(1){
            int blad=0;
            cin>>a;
            cin>>b;
            if(a>plansza.board.size() || b>plansza.board.size() || a<1 || b<1){
                cout<<"Nieprawidlowe pole!"<<endl;
                blad=1;          
            }
            if(blad==0){
                if(plansza.board[a-1][b-1]!='.'){
                    cout<<"Pole jest jest juz zajete, wybierz inne. "<<endl;
                    
                }
                if(plansza.board[a-1][b-1]=='.'){
                    plansza.board[a-1][b-1]=player;
                    break;
                }
            }   
        }
        }
    }       
    ZmienGracza(); //zmien po wyborze 
    
}



#endif
